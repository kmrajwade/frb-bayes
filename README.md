FRB-BAYES:  A generic (ish) Bayesian Inference framework to infer parameters of FRB populations.

Dependencies:
Needs boost libraries > 1.55.0

Installation:
The code uses cmake 2.0 and above for compilation. Here is an example below:

` git clone https://gitlab.com/kmrajwade/frb_bayes.git`


` cd frb_bayes; mkdir build; cd build`


` cmake -DBOOST_ROOT=<top level path to boost install> ../`


` make -j 16`

This will create an executable called `run_frb_bayes` that can be used to run the Bayesian inference.

TODO:

1. For now, the code takes Priors that could be variable and the User can build in new Priors. The code needs flexibility in terms of the LikeliHood function.



