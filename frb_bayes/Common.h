#ifndef FRB_BAYES_COMMON_H
#define FRB_BAYES_COMMON_H

#define BOOST_LOG_DYN_LINK 1
#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>

#include <cstddef>
#include <cmath>
#include <stdexcept>
#include <memory>
#include <string>
#include <iostream>
#include <string>
#include <sstream>
#include <iomanip>
#include <sys/types.h>

#endif //PSRDADA_CPP_COMMON_HPP
