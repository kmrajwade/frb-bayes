/* This is the posterior class. The posterior vector holds the probability as a function of various parameters
 * Templated to take any data type and any Prior Type
 */
#ifndef FRB_BAYES_POSTERIOR_H
#define FRB_BAYES_POSTERIOR_H


#include "frb_bayes/Common.h"
#include "frb_bayes/LikeliHood.h"

namespace frb_bayes
{
    template<typename LikeliHoodType1, typename LikeliHoodType2, typename PriorType1, typename PriorType2>
    class Posterior
    {
        public:
            typedef typename LikeliHoodType1::NumericalRep NumericalRep;

        public:
            Posterior(LikeliHoodType1& like1, LikeliHoodType2& like2, PriorType1& prior1, PriorType2& prior2);
            ~Posterior();

            void generate();

            void normalize();

            void marginalize(std::uint32_t axis);

            std::vector<std::vector<NumericalRep>> post();
            std::vector<NumericalRep> marginalized_post();

        private:
            LikeliHoodType1 _like1;
            LikeliHoodType2 _like2;
            PriorType1 _prior1;
            PriorType2 _prior2;
            std::vector<std::vector<NumericalRep>> _post;
            std::vector<NumericalRep> _marginalized_post;
    };
}
#include "frb_bayes/detail/Posterior.cpp"
#endif
