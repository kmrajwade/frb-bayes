/* Header that defines a Priors Class. The idea is that any model paramter can be
 * fed upon construction and a specific set of distributions are available for the
 * user to define the prior PDFs for their model parameters*/
#ifndef FRB_BAYES_BASEPRIOR_H
#define FRB_BAYES_BASEPRIOR_H


#include "frb_bayes/Common.h"
#include <algorithm>

namespace frb_bayes
{

template<typename NumRepType>
class BasePrior
{
    public:

        /*Constructor
         * Takes first and last range in which to generate samples
         * Takes the type of distribution to use to generate samples
         * Takes the number of samples that need to be generated*/
        BasePrior(double r1, double r2, std::size_t nsamples);
        ~BasePrior();

        double r1();
        double r2();

        std::size_t samples();

    private:
        float _r1;
        float _r2;
        std::size_t _nsamples;
};
}
#include"frb_bayes/detail/BasePrior.cpp"
#endif
