#include "frb_bayes/UniformPrior.h"

namespace frb_bayes
{
    template<typename NumRepType>
    UniformPrior<NumRepType>::UniformPrior(double r1, double r2, std::size_t nsamples)
    :BaseT(r1,r2,nsamples)
    {
        _dx = (r2 - r1)/nsamples;
    }

    template<typename NumRepType>
    UniformPrior<NumRepType>::~UniformPrior()
    {
    }

    template<typename NumRepType>
    NumRepType UniformPrior<NumRepType>::generate(double x)
    {
        return static_cast<NumRepType>(1.0/(BaseT::r2() - BaseT::r1()));
    }


    template<typename NumRepType>
    double UniformPrior<NumRepType>::dx()
    {
        return _dx;
    }
}
