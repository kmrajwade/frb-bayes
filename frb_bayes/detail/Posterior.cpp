



#include "frb_bayes/Posterior.h"

namespace frb_bayes
{

    template<typename LikeliHoodType1, typename LikeliHoodType2, typename PriorType1, typename PriorType2>
    Posterior<LikeliHoodType1, LikeliHoodType2, PriorType1, PriorType2>::Posterior( LikeliHoodType1& like1, LikeliHoodType2& like2, PriorType1& prior1, PriorType2& prior2)
    :_like1(like1)
    ,_like2(like2)
    ,_prior1(prior1)
    ,_prior2(prior2)
    {
        _post.resize(_prior1.samples());
    }

    template<typename LikeliHoodType1, typename LikeliHoodType2, typename PriorType1, typename PriorType2>
    Posterior<LikeliHoodType1, LikeliHoodType2, PriorType1, PriorType2>::~Posterior()
    {
    }

    template<typename LikeliHoodType1, typename LikeliHoodType2, typename PriorType1, typename PriorType2>
    void Posterior<LikeliHoodType1, LikeliHoodType2, PriorType1, PriorType2>::generate()
    {
        for (std::size_t ii = 0; ii < _prior1.samples(); ++ii)
        {
            for (std::size_t jj = 0; jj <= _prior2.samples(); ++jj)
            {
                _post[ii].push_back(_like1( _prior1.r1() + ii*_prior1.dx(), 1, _prior2.r1() + jj*_prior2.dx()) + _like2(_prior1.r1() + ii*_prior1.dx(), 1, _prior2.r1() + jj*_prior2.dx() ) + logl(_prior1.generate(_prior1.r1() + ii*_prior1.dx())) + logl(_prior2.generate(_prior2.r1() + jj*_prior2.dx())));
            }
        }
    }

    template<typename LikeliHoodType1, typename LikeliHoodType2, typename PriorType1, typename PriorType2>
    void Posterior<LikeliHoodType1, LikeliHoodType2, PriorType1, PriorType2>::normalize()
    {
        /* Find the normalizing factor */
        double norm = 0.0;
        for (std::size_t ii = 0 ; ii < _prior1.samples(); ++ii)
        {
            norm += std::accumulate( _post[ii].begin(), _post[ii].end(), 0);
        }

        /*Normalize*/
        for (std::size_t ii = 0; ii < _prior1.samples(); ++ii)
        {
            for (std::size_t jj = 0; jj <= _prior2.samples(); ++jj)
            {
                _post[ii][jj]/= norm;
            }
        }
    }

    template<typename LikeliHoodType1, typename LikeliHoodType2, typename PriorType1, typename PriorType2>
    void Posterior<LikeliHoodType1, LikeliHoodType2, PriorType1, PriorType2>::marginalize( std::uint32_t axis )
    {
        _marginalized_post.clear();

        if (axis != 1 && axis != 0)
        {
            return;
        }
        else if (axis == 1)
        {
            for (std::size_t ii = 0; ii < _prior1.samples(); ++ii)
            {
                _marginalized_post.push_back(std::accumulate( _post[ii].begin(), _post[ii].end(), 0.0));
            }
            /* Normalize*/
            double norm = std::accumulate(_marginalized_post.begin(), _marginalized_post.end(),0.0);
            for (std::size_t ii = 0; ii < _prior2.samples(); ++ii)
            {
                _marginalized_post[ii] /= norm;
            }
            return;
        }
        else
        {
            for (std::size_t ii = 0; ii < _prior2.samples(); ++ii)
            {
                double sum = 0.0;
                for (std::size_t jj = 0; jj < _prior1.samples(); ++jj)
                {
                    sum += _post[jj][ii];
                }
                _marginalized_post.push_back(sum);
            }
            /*Normalize*/
            double norm = std::accumulate(_marginalized_post.begin(), _marginalized_post.end(),0.0);
            for (std::size_t ii = 0; ii < _prior1.samples(); ++ii)
            {
                _marginalized_post[ii] /= norm;
            }
            return;
        }
    }

    template<typename LikeliHoodType1, typename LikeliHoodType2, typename PriorType1, typename PriorType2>
    std::vector<std::vector<typename Posterior<LikeliHoodType1, LikeliHoodType2, PriorType1, PriorType2>::NumericalRep>> Posterior<LikeliHoodType1, LikeliHoodType2, PriorType1, PriorType2>::post()
    {
        return _post;
    }

    template<typename LikeliHoodType1, typename LikeliHoodType2, typename PriorType1, typename PriorType2>
    typename std::vector<typename Posterior<LikeliHoodType1, LikeliHoodType2, PriorType1, PriorType2>::NumericalRep> Posterior<LikeliHoodType1, LikeliHoodType2, PriorType1, PriorType2>::marginalized_post()
    {
        return _marginalized_post;
    }
}
