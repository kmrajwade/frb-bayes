#include "frb_bayes/BasePrior.h"

namespace frb_bayes
{
    template<typename NumRepType>
    BasePrior<NumRepType>::BasePrior(double r1, double r2, std::size_t nsamples)
    :_r1(r1)
    ,_r2(r2)
    ,_nsamples(nsamples)
    {
    }

    template<typename NumRepType>
    BasePrior<NumRepType>::~BasePrior()
    {
    }

    template<typename NumRepType>
    double BasePrior<NumRepType>::r1()
    {
        return _r1;
    }

    template<typename NumRepType>
    double BasePrior<NumRepType>::r2()
    {
        return _r2;
    }

    template<typename NumRepType>
    std::size_t BasePrior<NumRepType>::samples()
    {
        return _nsamples;
    }

}
