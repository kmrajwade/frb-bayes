#include"frb_bayes/LikeliHood.h"
#include <math.h>



namespace frb_bayes
{
    template<typename NumRepType>
    LikeliHood<NumRepType>::LikeliHood(float Smax, float Smin, float Slim, std::size_t Nobs, float tf, float rate)
    :_Smax(Smax)
    ,_Slim(Slim)
    ,_Smin(Smin)
    ,_Nobs(Nobs)
    ,_tf(tf)
    ,_rate(rate)
    {
    }

    template<typename NumRepType>
    LikeliHood<NumRepType>::~LikeliHood()
    {
    }

    template<typename NumRepType>
    double LikeliHood<NumRepType>::logfactorial( std::size_t x)
    {
        double fact = 0.0;
        if (x == 0)
            return fact;
        else
        {
            for (std::size_t ii = x;ii > 0; --ii)
            {
                fact += logl(static_cast<double>(ii));
            }
            return fact;
        }
    }

    template<typename NumRepType>
    double LikeliHood<NumRepType>::factorial( std::size_t x)
    {
        double fact = 1.0;
        if ( x == 0)
            return fact;
        else
        {
            for (std::size_t ii = x; ii > 0; --ii)
            {
                fact *= static_cast<double>(ii);
            }
            return fact;
        }
    }


    template<typename NumRepType>
    float const& LikeliHood<NumRepType>::Smax()
    {
        return _Smax;
    }

    template<typename NumRepType>
    float const& LikeliHood<NumRepType>::Smin()
    {
        return _Smin;
    }

    template<typename NumRepType>
    float const& LikeliHood<NumRepType>::Slim()
    {
        return _Slim;
    }

    template<typename NumRepType>
    float const& LikeliHood<NumRepType>::tf()
    {
        return _tf;
    }

    template<typename NumRepType>
    float const& LikeliHood<NumRepType>::rate()
    {
        return _rate;
    }

    template<typename NumRepType>
    std::size_t const& LikeliHood<NumRepType>::Nobs()
    {
        return _Nobs;
    }

    template<typename NumRepType>
    NumRepType LikeliHood<NumRepType>::operator ()( std::size_t N, std::size_t k, float alpha )
    {

        double term1;
        term1 = logfactorial(N) - (logfactorial(_Nobs)) - (logfactorial(N - _Nobs));
        double term2 = 0.0;
        if (k > 1)
            term2 = logl(1.0 - (_rate*_tf*expl(-1.0*_rate*_tf)));
        else
            term2 = logl(_rate*_tf) - _rate*_tf;

        double term3 = logl((powl(_Smax, alpha + 1) - powl(_Slim, alpha+1))/(powl(_Smax, alpha+1) - powl(_Smin, alpha+1)));

        _like = static_cast<NumRepType>( term1 + _Nobs*term2 + _Nobs*term3  +  (N - _Nobs) * logl(1 - (powf(_rate * _tf, k) * std::exp(-1.0*_rate*_tf)/factorial(k))* (powl(_Smax, alpha + 1) - powl(_Slim, alpha+1))/(powf(_Smax, alpha+1) - powl(_Smin, alpha+1))));

        return _like;
    }
}
