#include "frb_bayes/NormalPrior.h"

namespace frb_bayes
{
    template<typename NumRepType>
    NormalPrior<NumRepType>::NormalPrior(double r1, double r2, std::size_t nsamples, double mean, double std_dev)
    :BaseT(r1,r2,nsamples)
    ,_mean(mean)
    , _std_dev(std_dev)
    {
        _dx = (BaseT::r2() - BaseT::r1())/nsamples;
    }

    template<typename NumRepType>
    NormalPrior<NumRepType>::~NormalPrior()
    {
    }

    template<typename NumRepType>
    NumRepType NormalPrior<NumRepType>::generate(double x)
    {
        return static_cast<NumRepType>(1.0/(std::sqrt(2.0*3.14*_std_dev)) * std::exp(std::pow(x - _mean,2)/(2.0 * std::pow(_std_dev,2))));
    }

    template<typename NumRepType>
    double NormalPrior<NumRepType>::dx()
    {
        return _dx;
    }
}
