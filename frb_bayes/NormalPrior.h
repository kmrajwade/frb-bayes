/* Header that defines a Priors Class. The idea is that any model paramter can be 
 * fed upon construction and a specific set of distributions are available for the
 * user to define the prior PDFs for their model parameters*/
#ifndef FRB_BAYES_NORMALPRIOR_H
#define FRB_BAYES_NORMALPRIOR_H


#include "frb_bayes/BasePrior.h"

namespace frb_bayes
{

template<typename NumRepType>
class NormalPrior: public BasePrior<NumRepType>
{
    typedef BasePrior<NumRepType> BaseT;
    public:

        /*Constructor
         * Takes first and last range in which to generate samples
         * Takes the type of distribution to use to generate samples
         * Takes the number of samples that need to be generated*/
        NormalPrior(double r1, double r2, std::size_t nsamples, double mean, double std_dev);
        ~NormalPrior();

        NumRepType generate(double x);

        double dx();

    private:
            double _mean;
            double _std_dev;
            double _dx;
};
}
#include"frb_bayes/detail/NormalPrior.cpp"
#endif
