#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <cstdio>
#include <string>
#include <fstream>
#include "frb_bayes/NormalPrior.h"
#include "frb_bayes/UniformPrior.h"
#include "frb_bayes/LikeliHood.h"
#include "frb_bayes/Posterior.h"
#include "boost/algorithm/string.hpp"
#include "boost/program_options.hpp"

namespace
{
  const size_t ERROR_IN_COMMAND_LINE = 1;
  const size_t SUCCESS = 0;
  const size_t ERROR_UNHANDLED_EXCEPTION = 2;
}

using namespace frb_bayes;

int main(int argc, char* argv[])
{
    try
    {
        std::size_t Ns;
        std::size_t Nr;
        double Nr1;
        double Nr2;
        std::size_t nN;
        double alphar1;
        double alphar2;
        std::size_t nalpha;
        std::string outfile;
        std::string prior;
        double Smax;
        double Smin;
        double Slim;
        double rate;
        double tf;

        /**
         * Define and parse the program options
         */
        namespace po = boost::program_options;
        po::options_description desc("Options");
        desc.add_options()
            ("help,h", "Print help messages")

            ("Nsingle, s", po::value<std::size_t>(&Ns)->default_value(1),
             "Number of single FRBs observed")

            ("Nrepeat,r", po::value<std::size_t>(&Nr)->default_value(1),
             "Number of repeating FRBs observed")

            ("output,o", po::value<std::string>(&outfile)->required(),
             "Name of the output base to save normalized posterior and marginalized normal posterior")

            ("Nrange1,r1", po::value<double> (&Nr1)->required(),
             "Start range of N")

            ("Nrange2,r2", po::value<double> (&Nr2)->required(),
             "End range of N")

            ("alphar1,a1", po::value<double> (&alphar1)->required(),
             "Start range for alpha")

            ("alphar2,a2", po::value<double> (&alphar2)->required(),
             "End range of alpha")

            ("nN,n", po::value<std::size_t> (&nN)->default_value(1),
             "number of values in the range")

            ("nalpha,a", po::value<std::size_t> (&nalpha)->default_value(1),
             "number of alpha values in the range")

            ("Smax,m", po::value<double> (&Smax)->required(),
             "Max flux in Jy")

            ("Slim,l", po::value<double> (&Slim)->required(),
             "Limiting Flux in Jy")

            ("Smin,n", po::value<double> (&Smin)->required(),
             "Min flux in Jy")

            ("rate,r", po::value<double> (&rate)->default_value(1),
             "Rate of repetition in bursts per hour")

            ("tfollowup,t", po::value<double> (&tf)->required(),
             "Follow up time in hours")

            ("prior,p", po::value<std::string> (&prior)->default_value("Uniform"),"Distribution of Prior");



        /* Catch Error and program description */
        po::variables_map vm;
        try
        {
            po::store(po::parse_command_line(argc, argv, desc), vm);
            if ( vm.count("help")  )
            {
                std::cout << "frb-bayes: A Bayesian Inference framework for constraining FRB source counts."
                    << std::endl << desc << std::endl;
                return SUCCESS;
            }
            po::notify(vm);
        }
        catch(po::error& e)

        {
            std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
            std::cerr << desc << std::endl;
            return ERROR_IN_COMMAND_LINE;
        }

        // CLI app over here
        if (prior == "Uniform")
        {
            UniformPrior<double> alphaprior(alphar1, alphar2, nalpha);
            UniformPrior<double> Nprior(Nr1, Nr2, nN);
            LikeliHood<double> sFRBLike( Smax, Smin, Slim, Ns, rate, tf);
            LikeliHood<double> rFRBLike( Smax, Smin, Slim, Nr, rate, tf);
            Posterior<decltype(sFRBLike), decltype(rFRBLike), decltype(Nprior), decltype(alphaprior)> post(sFRBLike, rFRBLike, Nprior, alphaprior);
            BOOST_LOG_TRIVIAL(info) << "Generating Posterior...";
            post.generate();
            BOOST_LOG_TRIVIAL(info) << "Normalizing 2-D Posterior...";
            post.normalize();
            /* Save Output */
            BOOST_LOG_TRIVIAL(info) << "Saving all Posterior outputs...";
            std::string outfilename = outfile + "_2Dpost.dat";
            std::ofstream outputfile(outfilename, std::ios::out | std::ios::binary);
            outputfile.write(reinterpret_cast<const char*>(post.post().data()), nalpha*nN*sizeof(double));
            outputfile.close();
            /* Marginalize over alpha*/
            BOOST_LOG_TRIVIAL(info) << "Marginalizing over alpha...";
            post.marginalize(1);
            outfilename = outfile + "_1Dalphapost.dat";
            outputfile.open(outfilename, std::ios::out | std::ios::binary);
            outputfile.write(reinterpret_cast<const char*>(post.marginalized_post().data()), post.marginalized_post().size()*sizeof(double));
            outputfile.close();
            /*Marginalize over N */
            BOOST_LOG_TRIVIAL(info) << "Marginalizing over N...";
            post.marginalize(0);
            outfilename = outfile + "_1DNpost.dat";
            outputfile.open(outfilename, std::ios::out | std::ios::binary);
            outputfile.write(reinterpret_cast<const char*>(post.marginalized_post().data()), post.marginalized_post().size()*sizeof(double));
            outputfile.close();
        }

        if (prior == "Normal")
        {
            NormalPrior<double> alphaprior(alphar1, alphar2, nalpha, -1.5, 1.0);
            UniformPrior<double> Nprior(Nr1, Nr2, nN);
            LikeliHood<double> sFRBLike( Smax, Smin, Slim, Ns, rate, tf);
            LikeliHood<double> rFRBLike( Smax, Smin, Slim, Nr, rate, tf);
            Posterior<decltype(sFRBLike), decltype(rFRBLike), decltype(Nprior), decltype(alphaprior)> post(sFRBLike, rFRBLike, Nprior, alphaprior);
            BOOST_LOG_TRIVIAL(info) << "Generating Posterior...";
            post.generate();
            BOOST_LOG_TRIVIAL(info) << "Normalizing 2-D Posterior...";
            post.normalize();
            /* Save Output */

            BOOST_LOG_TRIVIAL(info) << "Saving all Posterior outputs...";
            std::string outfilename = outfile + "_2Dpost.dat";
            std::ofstream outputfile(outfilename, std::ios::out | std::ios::binary);
            outputfile.write(reinterpret_cast<const char*>(post.post().data()), nalpha*nN*sizeof(double));
            outputfile.close();
            /* Marginalize over alpha*/
            post.marginalize(1);
            outfilename = outfile + "_1Dalphapost.dat";
            outputfile.open(outfilename, std::ios::out | std::ios::binary);
            outputfile.write(reinterpret_cast<const char*>(post.marginalized_post().data()), nalpha*sizeof(double));
            outputfile.close();
            /*Marginalize over N */
            post.marginalize(0);
            outfilename = outfile + "_1DNpost.dat";
            outputfile.open(outfilename, std::ios::out | std::ios::binary);
            outputfile.write(reinterpret_cast<const char*>(post.marginalized_post().data()), nN*sizeof(double));
            outputfile.close();
        }

    }

    catch(std::exception& e)
    {
        std::cerr << "Unhandled Exception reached the top of main: "
            << e.what() << ", application will now exit" << std::endl;
        return ERROR_UNHANDLED_EXCEPTION;
    }
    return SUCCESS;
}
