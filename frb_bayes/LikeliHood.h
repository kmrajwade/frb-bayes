/* Header that defines a LikeliHood Class. The idea is that any model paramter can be 
 * fed upon construction and a specific set of distributions are available for the
 * user to define the prior PDFs for their model parameters*/
#ifndef FRB_BAYES_LIKELIHOOD_H
#define FRB_BAYES_LIKELIHOOD_H


#include "frb_bayes/Common.h"
#include <algorithm>

namespace frb_bayes
{

template<typename NumRepType>
class LikeliHood
{
    public:

        /*Constructor
         * Takes multiple arguments
         * Smin
         * Smax
         * Slim
         * Number of one-off FRBs
         * Number of repeating FRBs
         * alpha
         * Follow-up survey time
         * Repeat Rate
         * Total number of sources
         */
        typedef NumRepType NumericalRep;

        LikeliHood(float Smin, float Smax, float Slim, std::size_t Nobs, float tf, float rate);
        ~LikeliHood();

        double factorial(std::size_t);

        double logfactorial(std::size_t);

        /* Value of Likelihood*/
        NumRepType operator()( std::size_t Nobs, std::size_t events, float alpha );

        /* Getters for all paramters */
        float const& Smin();

        float const& Smax();

        float const& Slim();

        std::size_t const& Nobs();

        float const& tf();

        float const& rate();

    private:
        float _Smin;
        float _Slim;
        float _Smax;
        std::size_t _Nobs;
        float _tf;
        float _rate;
        NumRepType _like;
};
}
#include"frb_bayes/detail/LikeliHood.cpp"
#endif
